:: Dans une fen�tre intel du bon type
@echo off

goto main

::-----------------------------------------------------------------------------
:: get Visual Studio version
::-----------------------------------------------------------------------------
:get_vs_version
    setlocal EnableExtensions EnableDelayedExpansion
    set _VER=%VisualStudioVersion%

    ::---  Filter main version number
    set _END=%_VER:*.=%
    call set _VER=%%_VER:%_END%=%%
    set _VER=%_VER:.=%

    ::---  MSVC version from VS version
    :: https://dev.to/yumetodo/list-of-mscver-and-mscfullver-8nd
    set _VS=Undefined
    if %_VER% == 11 set _VS="Visual Studio 11 2012"
    if %_VER% == 12 set _VS="Visual Studio 12 2013"
    if %_VER% == 14 set _VS="Visual Studio 14 2015"
    if %_VER% == 15 set _VS="Visual Studio 15 2017"
    if %_VER% == 16 set _VS="Visual Studio 16 2019"
    if %_VS% == Undefined (
        echo Unsupported Visual Studio Version: %_VER%
        exit /b 2
    )

    endlocal & set RET_1=%_VS%
goto :EOF


::-----------------------------------------------------------------------------
:: main()
::-----------------------------------------------------------------------------
:main
    setlocal enableextensions enabledelayedexpansion

    :: ---  Moveto build\win64
    if not exist build\win64 (mkdir build\win64)
    pushd build\win64

    :: ---  Get Visual Studio version
    call :get_vs_version & set _VS=!RET_1!
    if %errorLevel% NEQ 0 (exit /b )

    :: ---  Remove quotes
    :: https://ss64.com/nt/syntax-dequote.html
    set _VS=###%_VS%###
    set _VS=%_VS:"###=%
    set _VS=%_VS:###"=%
    set _VS=%_VS:###=%

    :: ---  Cmake
    cmake -G "%_VS%" -A x64 ..\..

    :: ---  msbuild
    msbuild /p:Configuration=Release INSTALL.vcxproj

    endlocal
goto :EOF
